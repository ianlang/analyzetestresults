import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):
    # Global definitions
    _FLOW_RATE_SIGNIFICANT_SAMPLES = 50
    _PRIME_STEP_MINIMUM_FLOW = 1
    _PRIME_STEP_LOW_FLOW_THRESHOLD = 10 # If too many samples are below the minimum flow, indicate an error
    # Read in data
    data = pandas.read_csv(CSVfileName)

    pressureChangesIndex = []
    # Check PID enable bit to detect start of usable data
    pressureChangesIndex.append([i for i, PID_en in enumerate(data['Press_PID_Enable (MANIFOLD)'].diff()) if PID_en == 1][0])
    # Detect location where pressure setpoint changes
    pressureChangesIndex.extend([i for i, pressure in enumerate(data['Press_PID_Setpt (MANIFOLD)'].diff()) if pressure != 0 and not math.isnan(pressure) and i > pressureChangesIndex[0]])
    # Check PID enable bit to detect end of usable data
    pressureChangesIndex.append([i for i, PID in enumerate(data['Press_PID_Enable (MANIFOLD)'].diff()) if PID == -1][-1])

    # Check if mobilizer flows during prime phase
    lowFlowCount = 0
    for flow in data['Sensirion_Liq_Flow (MANIFOLD)'][pressureChangesIndex[1] - _FLOW_RATE_SIGNIFICANT_SAMPLES : pressureChangesIndex[1] - 1]:
        if flow < _PRIME_STEP_MINIMUM_FLOW:
            lowFlowCount += 1
    if lowFlowCount > _PRIME_STEP_LOW_FLOW_THRESHOLD:
        print("WARNING: Detected low flow rate during prime phase")  

    # Find flow rate at each pressure setpoint
    pressureSetpoints = []
    flowRates = []
    i = 1
    print("Flow Rates:")
    for index in pressureChangesIndex[i:-1]:
        pressureSetpoint = data['Press_PID_Setpt (MANIFOLD)'][index]
        pressureSetpoints.append(pressureSetpoint)
        flowRateSubset = data['Sensirion_Liq_Flow (MANIFOLD)'][pressureChangesIndex[i+1] - _FLOW_RATE_SIGNIFICANT_SAMPLES : pressureChangesIndex[i+1] - 1]
        flowRate = statistics.median(flowRateSubset)
        flowRates.append(flowRate)
        print(f"\t{pressureSetpoint} mbar: {round(flowRate,2)} uL/min")
        i += 1

    # Plotting data
    plt.close('all')
    fig,axs = plt.subplots(nrows=2,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('Mobilizer Flow Rate Test')
    fig.suptitle("Mobilizer Flow Rate Test - " + os.path.basename(CSVfileName), fontsize=14)

    # Plot raw flow rate and pressure on subplot 0
    axs[0].set_title('Raw Flow Rate and Pressure')
    axs[0].set_ylabel('Flow Rate (uL/min)')
    axs[0].set_xlabel('Time (sec)')
    lns1 = axs[0].plot(data['Time (sec)'], data['Sensirion_Liq_Flow (MANIFOLD)'], 'C0', label = 'Flow Rate')
    # Place dots at appropriate points on flow rate graph
    flowRateAvgIndex = [i - _FLOW_RATE_SIGNIFICANT_SAMPLES/2 for i in pressureChangesIndex[2:]] 
    axs[0].plot(data['Time (sec)'][flowRateAvgIndex], flowRates, 'oC0')
    
    axs_pres = axs[0].twinx()
    axs_pres.set_ylabel('Pressure (mbar)')
    lns2 = axs_pres.plot(data['Time (sec)'], data['Press_Out (MANIFOLD)'], 'C1', label = 'Manifold Pressure')

    # Format legend
    lns = lns1+lns2
    labs = [x.get_label() for x in lns]
    plt.legend(lns, labs, loc='upper left', bbox_to_anchor=(0,1), bbox_transform=axs[0].transAxes)

    # Plot final flow rates vs pressure on subplot 1
    axs[1].set_title('Flow Rate Results')
    axs[1].set_ylabel('Flow Rate (uL/min)')
    axs[1].set_xlabel('Pressure (mbar)')
    axs[1].plot(pressureSetpoints, flowRates, 'o--', label = "Flow Rate")
    axs[1].legend()

    plt.show()
    print("\n")

    # Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Mobilizer flow rate test results')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)