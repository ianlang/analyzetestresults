import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):
    # Global definitions
    _CHANNEL_NAME = ["Anolyte", "Catholyte", "Mobilizer"]
    _CHANNEL_SETPOINT_HEADERS = ['Vset_Anolyte (HV)', 'Vset_Catholyte (HV)', 'Vset_Mobilizer (HV)']
    _CHANNEL_VOLTAGE_HEADERS = ['Vmon_Anolyte_Ave (HV)', 'Vmon_Catholyte_Ave (HV)', 'Vmon_Mobilizer_Ave (HV)']
    _CHANNEL_CURRENT_HEADERS = ['Imon_Anolyte_Ave (HV)', 'Imon_Catholyte_Ave (HV)', 'Imon_Mobilizer_Ave (HV)']
    _SETTLING_TIME = 75     # Number of samples to wait for voltage and current to settle

    # Read in data
    data = pandas.read_csv(CSVfileName)

    # Create plot
    plt.close('all')
    fig,axs = plt.subplots(nrows=3,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('HV Range Test')
    fig.suptitle("HV Range Test - " + os.path.basename(CSVfileName), fontsize=14)

    # Loop through the 3 channels
    for channel in range(3):
        print(f"{_CHANNEL_NAME[channel]} Channel:")

        # Find locations where voltage changes
        voltageChangeLocs = [i for i, dv in enumerate(data[_CHANNEL_SETPOINT_HEADERS[channel]].diff()) if dv != 0 and not math.isnan(dv)]

        testResults = pandas.DataFrame(columns = ['Setpoint', 'Voltage', 'Current', 'Voltage Error'])
        for change in voltageChangeLocs:
            setpoint = data[_CHANNEL_SETPOINT_HEADERS[channel]][change + _SETTLING_TIME]
            voltage = data[_CHANNEL_VOLTAGE_HEADERS[channel]][change + _SETTLING_TIME]
            current = data[_CHANNEL_CURRENT_HEADERS[channel]][change + _SETTLING_TIME]
            voltageError = voltage - setpoint

            testResults.loc[len(testResults)] = [setpoint, voltage, current, voltageError]
            #print(f"\tSetpoint: {round(setpoint,2)}, Voltage = {round(voltage,2)} V, Current = {round(current,3)} uA")

        testResults = testResults.sort_values(by=['Setpoint'], ignore_index=True) # Organize so setpoints are in numerical order
        print(f"\tVoltage Limits: {round(testResults['Voltage'].iloc[0],1)}V min, {round(testResults['Voltage'].iloc[-1],1)}V max")
        print(f"\tMax Current Leakage: {round(max(abs(testResults['Current'])),2)}uA")

        # Do not plot error at 0V for anolyte channel as it is expected to be ~300V off
        if channel == 0:
            testResults = testResults[1:]

        print(f"\tMax Voltage Error: {round(max(abs(testResults['Voltage Error'])),2)}V")

        # Plot voltage error
        axs[channel].set_title(_CHANNEL_NAME[channel])
        axs[channel].set_ylabel('Voltage Error (V)')
        axs[channel].set_xlabel('Voltage Setpoint (V)')
        lns1 = axs[channel].plot(testResults['Setpoint'], testResults['Voltage Error'], 'C0-', label = 'Voltage Error')

        # Plot measured current
        curr_axs = axs[channel].twinx()
        curr_axs.set_ylabel('Current (uA)')
        lns2 = curr_axs.plot(testResults['Setpoint'], testResults['Current'], 'C1--', label = 'Current')
        lns = lns1 + lns2
        labs = [x.get_label() for x in lns]
        plt.legend(lns, labs)# loc='upper left', bbox_to_anchor=(0,1), bbox_transform=axs[0].transAxes)
        
    plt.show()
    print("\n")

# Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='HV range test results')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)