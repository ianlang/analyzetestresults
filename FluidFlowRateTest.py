import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):
    # Global definitions
    _CHANNEL_VALVE_STATES = [1,2,3,4,6,7]                       # Indicates start and end of each test
    _CHANNEL_NAMES = ["Anolyte", "Catholyte", "Mobilizer"]
    _CHANNEL_START_BUFFER = [100,100,200]                       # Number of samples to ignore as start of each channel test
    _PLOT_Y_BUFFER = 0.01                                       # Insert 1% buffer on plot y-limits
    _FLOW_ERR_TOL_MAG = 0.1                                     # 10% magnitude error tolerance
    _FLOW_ERR_TOL_DUTY = 0.05                                   # Allowable errors no more than 5% over period
    _FLOW_ERR_TOL_PERIOD = 100                                  # 100 sample period to detect errors         

    # Read in data, convert valve mask from hex to integer
    data = pandas.read_csv(CSVfileName, converters={'Pneum_Valve_Mask_En (MANIFOLD)': lambda x: int(x, 16)})

    # Only look at applicable valve states (ignore changes due to ballast recharging)
    data['Pneum_Valve_Mask_En (MANIFOLD)'] = [x & 0xFE for x in data['Pneum_Valve_Mask_En (MANIFOLD)']]

    # Detect location where valve state changes
    valveChangesIndex = [i for i, valve in enumerate(data['Pneum_Valve_Mask_En (MANIFOLD)'].diff()) if valve != 0 and not math.isnan(valve)]

    # Set up plot
    plt.close('all')
    fig,axs = plt.subplots(nrows=3,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('Fluid Flow Rate Test')
    fig.suptitle("Fluid Flow Rate Test - " + os.path.basename(CSVfileName), fontsize=14)

    print("Flow Rates:")
    flowRateMedians = []
    maxDeviations = []
    # Loop through each channel
    for i in [0,1,2]:
        # Calculate flow rate median and max deviation
        flowRateData = data['Sensirion_Liq_Flow (MANIFOLD)'][valveChangesIndex[_CHANNEL_VALVE_STATES[2*i]] + _CHANNEL_START_BUFFER[i] : valveChangesIndex[_CHANNEL_VALVE_STATES[2*i + 1]]]
        flowRateMedian = statistics.median(flowRateData)
        flowRateMax = flowRateMedian    # Inital value
        flowRateMin = flowRateMedian    # Inital value

        # Iterate through flow rates to see if certain interruptions in flow are allowable errors (caused by small bubbles)
        for index, flowRate in enumerate(flowRateData):
            if abs((flowRate - flowRateMedian) / flowRateMedian) > _FLOW_ERR_TOL_MAG:                   # Check if flow rate is outside tolerance
                endLoc = index + _FLOW_ERR_TOL_PERIOD
                if endLoc < len(flowRateData):                                                          # Make sure index is within bounds        
                    badFlowCount = 0                   
                    for flowRate_sub in flowRateData[index:endLoc]:                                     # Count number of flow rate errors in local subsection
                        if abs((flowRate_sub - flowRateMedian) / flowRateMedian) > _FLOW_ERR_TOL_MAG:
                            badFlowCount = badFlowCount + 1                      
                    if badFlowCount / _FLOW_ERR_TOL_PERIOD < _FLOW_ERR_TOL_DUTY:                        # Check if number of errors is allowable
                        continue                                                                        # If allowable, do not update min or max values            
            # Update min and max values if applicable
            if flowRate > flowRateMax:
                flowRateMax = flowRate
            elif flowRate < flowRateMin:
                flowRateMin = flowRate

        maxDeviation = max(flowRateMedian - flowRateMin, flowRateMax - flowRateMedian) / flowRateMedian * 100

        flowRateMedians.append(flowRateMedian)
        maxDeviations.append(maxDeviation)

        # Print channel output
        print(f"\t{_CHANNEL_NAMES[i]}:\t{round(flowRateMedian,2)} uL/min, {round(maxDeviation,1)}% maximum deviation from median")
        if(maxDeviation > (_FLOW_ERR_TOL_MAG * 100)):
            print(f"\nWarning: Abnormal Flow Detected on {_CHANNEL_NAMES[i]} Channel\n")
        # Plot channel data
        startTime = data['Time (sec)'][valveChangesIndex[_CHANNEL_VALVE_STATES[2*i]] + _CHANNEL_START_BUFFER[i]]
        endTime = data['Time (sec)'][valveChangesIndex[_CHANNEL_VALVE_STATES[2*i+1]]]
        fitData = pandas.DataFrame(data={'Time (sec)':[startTime, endTime], 'Flow Rate Median':[flowRateMedian, flowRateMedian]})

        axs[i].set_title(_CHANNEL_NAMES[i] + ' Flow Rate')
        axs[i].set_ylabel('Flow Rate (uL/min)')
        axs[i].set_xlabel('Time (sec)')
        axs[i].set_xlim(startTime, endTime)
        axs[i].set_ylim(flowRateMin - flowRateMedian * _PLOT_Y_BUFFER, flowRateMax + flowRateMedian * _PLOT_Y_BUFFER)

        axs[i].plot(data['Time (sec)'], data['Sensirion_Liq_Flow (MANIFOLD)'], 'C0', label = 'Flow Rate')
        axs[i].plot(fitData['Time (sec)'], fitData['Flow Rate Median'], 'C1--', label = 'Flow Rate Median')
        
        axs[i].legend()

    plt.show()
    print("\n")

    # Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fluid flow rate test results')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)