import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):

    # Global definitions
    _GENERIC_TRANSITION_BUFFER = 10
    _ORIFICE_TEST_TRANSITION_BUFFER = 10          # number of samples to throw away before and after pump turns off
    _BALLAST_TEST_TRANSITION_BUFFER = 2000
    _MANIFOLD_TEST_TRANSITION_BUFFER = 100
    _SMALL_ORIFICE_TEST_START_INDEX = 1
    _SMALL_ORIFICE_TEST_END_INDEX = 2
    #_LARGE_ORIFICE_TEST_START_INDEX = 2
    #_LARGE_ORIFICE_TEST_END_INDEX = 3
    _BALLAST_TEST_START_INDEX = 2
    _BALLAST_TEST_END_INDEX = 3
    _MANIFOLD_TEST_START_INDEX = 4
    _MANIFOLD_TEST_END_INDEX = 5

    # Read in data, convert valve mask from hex to integer
    data = pandas.read_csv(CSVfileName, converters={'Pneum_Valve_Mask_En (MANIFOLD)': lambda x: int(x, 16)})

    # Find transitions between test stages
    stageChanges = [x for x, i in enumerate(data['Press_PID_Enable (MANIFOLD)'].diff()) if i != 0]

    # Find where valves change states
    valveStateChanges = [] 
    test = []
    for i, change in enumerate(data['Pneum_Valve_Mask_En (MANIFOLD)'].diff()):
        if change != 0:
            valveStateChanges.append(i)
            test.append(change)
            if (change == 3 or change == 4) and i > stageChanges[_SMALL_ORIFICE_TEST_START_INDEX] and i < stageChanges[_SMALL_ORIFICE_TEST_END_INDEX]:
                stageChanges.insert(_SMALL_ORIFICE_TEST_START_INDEX + 1,i)   
            elif i > stageChanges[-1]:
                stageChanges.append(i)
                break

    orificeLeakDurationMinutes = round((data['Time (sec)'][stageChanges[_SMALL_ORIFICE_TEST_END_INDEX]] - data['Time (sec)'][stageChanges[_SMALL_ORIFICE_TEST_START_INDEX]])/60,0)

    # Calculate leak rate during orifice test
    smallOrificeNumRecharges = 0
    smallOrificeLeakRate = []
    #largeOrificeNumRecharges = 0
    #largeOrificeLeakRate = []
    for i, change in enumerate(valveStateChanges):
        if(change > stageChanges[_SMALL_ORIFICE_TEST_START_INDEX] and change < (stageChanges[_SMALL_ORIFICE_TEST_END_INDEX])): # Small orifice test
            valveState = data['Pneum_Valve_Mask_En (MANIFOLD)'][change]
            if valveState == 0x07:       # Pump turns on (0x07 for beta-3)
                smallOrificeNumRecharges += 1
            elif valveState == 0x06:     # Pump turns off (0x06 for beta-3)
                if(valveStateChanges[i+1] < stageChanges[_SMALL_ORIFICE_TEST_END_INDEX]):  # Make sure the next transition is still within test stage
                    startBallastPres = data['Press_Ballast (MANIFOLD)'][change + _ORIFICE_TEST_TRANSITION_BUFFER]
                    startBallastTime = data['Time (sec)'][change + _ORIFICE_TEST_TRANSITION_BUFFER]
                    endBallastPres = data['Press_Ballast (MANIFOLD)'][valveStateChanges[i+1] - _ORIFICE_TEST_TRANSITION_BUFFER]
                    endBallastTime = data['Time (sec)'][valveStateChanges[i+1] - _ORIFICE_TEST_TRANSITION_BUFFER]
                    smallOrificeLeakRate.append((startBallastPres - endBallastPres) / (startBallastTime - endBallastTime))

        # if(change > stageChanges[_LARGE_ORIFICE_TEST_START_INDEX] and change < (stageChanges[_LARGE_ORIFICE_TEST_END_INDEX])): # Large orifice test
        #     valveState = data['Pneum_Valve_Mask_En (MANIFOLD)'][change]
        #     if valveState == 0x0B:       # Pump turns on (0x0B for beta-3)
        #         largeOrificeNumRecharges += 1
        #     elif valveState == 0x0A:     # Pump turns off (0x0A for beta-3)
        #         if(valveStateChanges[i+1] < stageChanges[_LARGE_ORIFICE_TEST_END_INDEX]):  # Make sure the next transition is still within test stage
        #             startBallastPres = data['Press_Ballast (MANIFOLD)'][change + _ORIFICE_TEST_TRANSITION_BUFFER]
        #             startBallastTime = data['Time (sec)'][change + _ORIFICE_TEST_TRANSITION_BUFFER]
        #             endBallastPres = data['Press_Ballast (MANIFOLD)'][valveStateChanges[i+1] - _ORIFICE_TEST_TRANSITION_BUFFER]
        #             endBallastTime = data['Time (sec)'][valveStateChanges[i+1] - _ORIFICE_TEST_TRANSITION_BUFFER]
        #             largeOrificeLeakRate.append((startBallastPres - endBallastPres) / (startBallastTime - endBallastTime))  

    avgSmallOrificeLeakRate = round(statistics.mean(smallOrificeLeakRate),2)    # Calculate average leak
    #avgLargeOrificeLeakRate = round(statistics.mean(largeOrificeLeakRate),2)    # Calculate average leak


    # Calculate leak rate during ballast test
    startBallastPres = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_START_INDEX] + _BALLAST_TEST_TRANSITION_BUFFER]
    startBallastTime = data['Time (sec)'][stageChanges[_BALLAST_TEST_START_INDEX] + _BALLAST_TEST_TRANSITION_BUFFER]
    endBallastPres = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_END_INDEX] - _GENERIC_TRANSITION_BUFFER]
    endBallastTime = data['Time (sec)'][stageChanges[_BALLAST_TEST_END_INDEX] - _GENERIC_TRANSITION_BUFFER]
    avgBallastLeakRate = round((startBallastPres - endBallastPres) / (startBallastTime - endBallastTime),3)

    # Calculate leak rate during manifold test
    startManifoldPres = data['Press_Out (MANIFOLD)'][stageChanges[_MANIFOLD_TEST_START_INDEX] + _MANIFOLD_TEST_TRANSITION_BUFFER]
    startManifoldTime = data['Time (sec)'][stageChanges[_MANIFOLD_TEST_START_INDEX] + _MANIFOLD_TEST_TRANSITION_BUFFER]
    endManifoldPres = data['Press_Out (MANIFOLD)'][stageChanges[_MANIFOLD_TEST_END_INDEX] - _MANIFOLD_TEST_TRANSITION_BUFFER]
    endManifoldTime = data['Time (sec)'][stageChanges[_MANIFOLD_TEST_END_INDEX] - _MANIFOLD_TEST_TRANSITION_BUFFER]
    avgManifoldLeakRate = round((startManifoldPres - endManifoldPres) / (startManifoldTime - endManifoldTime),3)

    # Print out results to console
    print(f'System recharged ballast {smallOrificeNumRecharges} times during {orificeLeakDurationMinutes} minute small orifice test.') 
    print(f'Small orifice leak rate: {abs(avgSmallOrificeLeakRate)} mbar / sec')
    #print(f'System recharged ballast {largeOrificeNumRecharges} times during {orificeLeakDurationMinutes} minute large orifice test.') 
    #print(f'Large orifice leak rate: {abs(avgLargeOrificeLeakRate)} mbar / sec')
    print(f'Ballast leak rate: {abs(avgBallastLeakRate)} mbar / sec')
    print(f'Manifold leak rate: {abs(avgManifoldLeakRate)} mbar / sec')


    ballastUpperLimit = data['Press_Ballast_Upper (MANIFOLD)'][stageChanges[_SMALL_ORIFICE_TEST_START_INDEX]]
    ballastLowerLimit = data['Press_Ballast_Lower (MANIFOLD)'][stageChanges[_SMALL_ORIFICE_TEST_START_INDEX]]

    ballastTestMinPressure = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_START_INDEX]:stageChanges[_BALLAST_TEST_END_INDEX]].min()
    ballastTestMaxPressure = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_START_INDEX]:stageChanges[_BALLAST_TEST_END_INDEX]].max()

    manifoldTestMinPressure = data['Press_Out (MANIFOLD)'][stageChanges[_MANIFOLD_TEST_START_INDEX]:stageChanges[_MANIFOLD_TEST_END_INDEX]].min()
    manifoldTestMaxPressure = data['Press_Out (MANIFOLD)'][stageChanges[_MANIFOLD_TEST_START_INDEX]:stageChanges[_MANIFOLD_TEST_END_INDEX]].max()

    # Plotting data
    plt.close('all')
    fig,axs = plt.subplots(nrows=3,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('Standalone Manifold Test')
    fig.suptitle("Standalone Manifold Test - " + os.path.basename(CSVfileName), fontsize=14)

    # Plot small orifice test on subplot 0
    axs[0].set_title('Small Orifice Leak Test')
    axs[0].set_ylabel('Pressure (mbar)')
    axs[0].set_xlabel('Time (sec)')
    data.plot(
        ax = axs[0], 
        x='Time (sec)', 
        y ='Press_Ballast (MANIFOLD)', 
        xlim = [data.at[stageChanges[_SMALL_ORIFICE_TEST_START_INDEX],'Time (sec)'], data.at[stageChanges[_SMALL_ORIFICE_TEST_END_INDEX],'Time (sec)']], 
        ylim = [ballastLowerLimit - 100, ballastUpperLimit + 100],
        label='Ballast Pressure'
    )
    axs[0].legend(loc='upper right')
    axs[0].get_xaxis().get_label().set_visible(False) # Hide x-axis label

    # Plot large orifice test on subplot 1
    # axs[1].set_title('Large Orifice Leak Test')
    # axs[1].set_ylabel('Pressure (mbar)')
    # axs[1].set_xlabel('Time (sec)')
    # data.plot(
    #     ax = axs[1], 
    #     x='Time (sec)', 
    #     y ='Press_Ballast (MANIFOLD)', 
    #     xlim = [data.at[stageChanges[_LARGE_ORIFICE_TEST_START_INDEX],'Time (sec)'], data.at[stageChanges[_LARGE_ORIFICE_TEST_END_INDEX],'Time (sec)']], 
    #     ylim = [ballastLowerLimit - 100, ballastUpperLimit + 100],
    #     label='Ballast Pressure'
    # )
    # axs[1].legend(loc='upper right')
    # axs[1].get_xaxis().get_label().set_visible(False) # Hide x-axis label

    # Plot ballast test on subplot 1
    axs[1].set_title('Ballast Leak Test')
    axs[1].set_ylabel('Pressure (mbar)')
    axs[1].set_xlabel('Time (sec)')
    data.plot(
        ax = axs[1], 
        x='Time (sec)', 
        y ='Press_Ballast (MANIFOLD)', 
        xlim = [data.at[stageChanges[_BALLAST_TEST_START_INDEX],'Time (sec)'], data.at[stageChanges[_BALLAST_TEST_END_INDEX],'Time (sec)']], 
        ylim = [ballastTestMinPressure - 2, ballastTestMaxPressure + 2],
        label = 'Ballast Pressure'
    )
    ballastData = pandas.DataFrame(data={'Time (sec)':[startBallastTime, endBallastTime], 'Pressure':[startBallastPres, endBallastPres]})
    ballastData.plot(
        ax = axs[1],
        x='Time (sec)',
        y='Pressure',
        label='Ballast Pressure (fitted)'
    )
    axs[1].legend(loc='upper right')
    axs[1].get_xaxis().get_label().set_visible(False) # Hide x-axis label

    # Plot manifold test on subplot 2
    axs[2].set_title('Manifold Leak Test')
    axs[2].set_ylabel('Pressure (mbar)')
    axs[2].set_xlabel('Time (sec)')
    data.plot(
        ax = axs[2], 
        x = 'Time (sec)', 
        y ='Press_Out (MANIFOLD)', 
        xlim = [data.at[stageChanges[_MANIFOLD_TEST_START_INDEX],'Time (sec)'], data.at[stageChanges[_MANIFOLD_TEST_END_INDEX],'Time (sec)']], 
        ylim = [manifoldTestMinPressure - 2, manifoldTestMaxPressure + 2],
        label = 'Output Pressure'
    )
    manifoldData = pandas.DataFrame(data={'Time (sec)':[startManifoldTime, endManifoldTime], 'Pressure':[startManifoldPres, endManifoldPres]})
    manifoldData.plot(
        ax = axs[2],
        x='Time (sec)',
        y='Pressure',
        label='Output Pressure (fitted)'
    )
    axs[2].legend(loc='upper right')

    plt.show()

    print("\n")


# Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analyze Manifold Performance')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)