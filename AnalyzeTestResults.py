# Test scripts
import ManifoldLeakTest
import GantryBurnInTest
import MobilizerFlowRateTest
import UVPickoffTest
import FluidFlowRateTest
import SampleFlowTest
import HVRangeTest
import ManifoldLeakTestInUnit

import sys
import argparse
import pandas
import zlib

# Unique identifiers for types of tests to run
_GANTRY_TEST_HASH = 829670282
_MANIFOLD_TEST_HASH = 2780659523
_MANIFOLD_IN_UNIT_TEST_HASH = 3526328693
_MOBILIZER_FLOW_TEST_HASH = 711952072
_UV_PICKOFF_TEST_HASH = 2781645471
_FLUID_FLOW_TEST_HASH = 811915853
_SAMPLE_FLOW_TEST_HASH = 3725975415
_HV_RANGE_TEST_HASH = 4058323183

# Parse arguments to extract CSV file name
parser = argparse.ArgumentParser(description='Test result CSV file')
parser.add_argument('inputCSV', type=argparse.FileType('r'))
args = parser.parse_args()
CSVfileName = args.inputCSV.name

# Read in column headers and calculate crc32 to identify proper test to run
CSVfile = pandas.read_csv(CSVfileName, nrows=0)
columns = CSVfile.columns.to_numpy('str')
if columns[-1].find('Unnamed:') != -1:
    columns = columns[:-1]
columnHash = zlib.crc32(bytearray(columns)) & 0xffffffff


#print(columnHash)

if columnHash == _MANIFOLD_TEST_HASH:
    ManifoldLeakTest.processTest(CSVfileName)
elif columnHash == _GANTRY_TEST_HASH:
    GantryBurnInTest.processTest(CSVfileName)
elif columnHash == _MOBILIZER_FLOW_TEST_HASH:
    MobilizerFlowRateTest.processTest(CSVfileName)
elif columnHash == _UV_PICKOFF_TEST_HASH:
    UVPickoffTest.processTest(CSVfileName)
elif columnHash == _FLUID_FLOW_TEST_HASH:
    FluidFlowRateTest.processTest(CSVfileName)
elif columnHash == _SAMPLE_FLOW_TEST_HASH:
    SampleFlowTest.processTest(CSVfileName)
elif columnHash == _HV_RANGE_TEST_HASH:
    HVRangeTest.processTest(CSVfileName)
elif columnHash == _MANIFOLD_IN_UNIT_TEST_HASH:
    ManifoldLeakTestInUnit.processTest(CSVfileName)
else:
    print("ERROR: Test result format not recognized.")

print ("\nPress ENTER to exit...")
input()
