import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):
    # Global definitions
    _MOTOR_NAME = ["X", "Y", "Z"]
    _MOTOR_POSITION_HEADERS = ['MOTX_Position (MAIN)', 'MOTY_Position (OPTICS)', 'MOTZ_Position (MAIN)']
    _MOTOR_LIMIT_HEADERS = ['MOTX_Limit_Sensor (MAIN)', 'MOTY_Limit_Sensor (OPTICS)', 'MOTZ_Limit_Sensor (MAIN)']
    _NUM_MOTOR_TRAVELS = 9

    # Read in data, convert limit sensors from hex to integer
    data = pandas.read_csv(CSVfileName, converters={
        'MOTX_Limit_Sensor (MAIN)': lambda x: int(x, 16), 
        'MOTY_Limit_Sensor (OPTICS)': lambda x: int(x, 16), 
        'MOTZ_Limit_Sensor (MAIN)': lambda x: int(x, 16)})

    # Loop through the 3 axes and calculate travel length and motor speed
    axisXBounds = []
    for axis in range(3):
        limitChangesIndex = []
        limitChanges = []
        motorSpeed = []
        motorTravel = []

        # Find locations where limit flag changes state
        for i, limit in enumerate(data[_MOTOR_LIMIT_HEADERS[axis]].diff()):
            if limit != 0 and not math.isnan(limit):
                limitChanges.append(limit)
                # Subtract 1 from index when motor moves off flag to ensure consistant starting location
                if limit < 0:
                    limitChangesIndex.append(i-1)
                else:
                    limitChangesIndex.append(i)
        
        # Calculate speed and travel distance
        i = 1
        while i < len(limitChanges):
            if limitChanges[i:i+2] == [-1, 2] or limitChanges[i:i+2] == [-2, 1]: # Make sure limit flag sequence is valid
                x1 = data[_MOTOR_POSITION_HEADERS[axis]][limitChangesIndex[i]]
                x2 = data[_MOTOR_POSITION_HEADERS[axis]][limitChangesIndex[i+1]]
                t1 = data['Time (sec)'][limitChangesIndex[i]]
                t2 = data['Time (sec)'][limitChangesIndex[i+1]]
                motorSpeed.append(abs((x1 - x2) / (t1 - t2)))
                motorTravel.append(abs(x1 - x2))
                i = i + 2
            else:
                print("ERROR: Failed to detect limits")
                i = i + 1

        # Error checking for number of detected travels
        if len(motorSpeed) != _NUM_MOTOR_TRAVELS:
            print(f"ERROR: Detected {len(motorSpeed)} travels on {_MOTOR_NAME[axis]}-axis, {_NUM_MOTOR_TRAVELS} expected")

        # Calculate average and standard deviation
        speedAvg = statistics.mean(motorSpeed)
        speedStDev = statistics.stdev(motorSpeed, speedAvg)
        travelAvg = statistics.mean(motorTravel)
        travelStDev = statistics.stdev(motorTravel, travelAvg)
        
        # Format and log data to console
        print(f"{_MOTOR_NAME[axis]}-axis:")
        print(f"\tAverage Speed: {round(speedAvg,2)} mm/s (Std Dev: {round(speedStDev,4)})")
        print(f"\tAverage Travel: {round(travelAvg,2)} mm (Std Dev: {round(travelStDev,4)})")

        # Save start and end location of axis motion for plot formatting purposes
        axisXBounds.append([data['Time (sec)'][limitChangesIndex[0]], data['Time (sec)'][limitChangesIndex[-1]]])

    # Plotting data
    plt.close('all')
    fig,axs = plt.subplots(nrows=3,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('Gantry Test ')
    fig.suptitle("Gantry Test - " + os.path.basename(CSVfileName), fontsize=14)

    # Plot x motor on subplot 0
    axs[0].set_title('X Motor')
    axs[0].set_ylabel('Position (mm)')
    axs[0].set_xlabel('Time (sec)')
    data.plot(
        ax = axs[0], 
        x='Time (sec)', 
        y ='MOTX_Position (MAIN)', 
        xlim = axisXBounds[0]
    )
    axs[0].legend(loc='upper right')

    # Plot y motor on subplot 1
    axs[1].set_title('Y Motor')
    axs[1].set_ylabel('Position (mm)')
    axs[1].set_xlabel('Time (sec)')
    data.plot(
        ax = axs[1], 
        x='Time (sec)', 
        y ='MOTY_Position (OPTICS)', 
        xlim = axisXBounds[1]
    )
    axs[1].legend(loc='upper right')

    # Plot z motor on subplot 2
    axs[2].set_title('Z Motor')
    axs[2].set_ylabel('Position (mm)')
    axs[2].set_xlabel('Time (sec)')
    data.plot(
        ax = axs[2], 
        x='Time (sec)', 
        y ='MOTZ_Position (MAIN)', 
        xlim = axisXBounds[2] 
    )
    axs[2].legend(loc='upper right')

    plt.show()
    print("\n")

# Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Gantry burn-in test results')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)