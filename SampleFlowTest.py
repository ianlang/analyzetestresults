import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):
    # Global definitions
    _FLOW_START_BUFFER = 75             # Number of samples to wait at start of flow
    _NUM_CAL_CHECK_STEPS = 5            # Number of steps to check calibration
    _ROT_VLV_POS_TOL = 5                # Tolerance of rotary valve position
    _ROT_VLV_FLOW_POS = 270             # Rotary valve position that allows flow
    _SAMPLE_FLOW_ON_MIN_VALUE = 2       # Minimum flow rate when in flow position
    _SAMPLE_FLOW_OFF_MAX_VALUE = 0.5    # Maximum flow rate when not in position

    _FLOW_ERR_TOL_MAG = 0.1             # 10% magnitude error tolerance
    _FLOW_ERR_TOL_DUTY = 0.05           # Allowable errors no more than 5% over period
    _FLOW_ERR_TOL_PERIOD = 100          # 100 sample period to detect errors            

    _PLOT_Y_BUFFER = 0.01

    # Read in data, convert valve mask from hex to integer
    data = pandas.read_csv(CSVfileName, converters={'Pneum_Valve_Mask_En (MANIFOLD)': lambda x: int(x, 16)})

    # Only look at applicable valve states (ignore changes due to ballast recharging)
    data['Pneum_Valve_Mask_En (MANIFOLD)'] = [x & 0xFE for x in data['Pneum_Valve_Mask_En (MANIFOLD)']]

    # Detect location where valve state changes
    valveChangesIndex = [i for i, valve in enumerate(data['Pneum_Valve_Mask_En (MANIFOLD)'].diff()) if valve != 0 and not math.isnan(valve)]

    # Check rotary valve calibration
    print("Rotary Valve Calibration Check:")
    rotVlvCalOk = 1
    for i in range(0,_NUM_CAL_CHECK_STEPS):
        flowData = data['Sensirion_Liq_Flow (MANIFOLD)'][valveChangesIndex[2*i] + _FLOW_START_BUFFER : valveChangesIndex[2*i + 1]]
        flowDataMedian = statistics.median(flowData)

        rotVlvPos = data['ROT_VLV_Position (OPTICS)'][valveChangesIndex[2*i]]
        rotVlvSetPos = data['ROT_VLV_Abs_Move (OPTICS)'][valveChangesIndex[2*i]]
        if abs(rotVlvPos - rotVlvSetPos) > _ROT_VLV_POS_TOL:
            print(f"Error: Rotary valve position is {rotVlvPos}°, should be {rotVlvSetPos}°")

        print(f"\tPosition {rotVlvSetPos}°: {round(flowDataMedian,2)} uL/min")

        if rotVlvSetPos == _ROT_VLV_FLOW_POS:
            if flowDataMedian < _SAMPLE_FLOW_ON_MIN_VALUE:
                rotVlvCalOk = 0  
        else:
            if flowDataMedian > _SAMPLE_FLOW_OFF_MAX_VALUE:
                rotVlvCalOk = 0

    if rotVlvCalOk == 1:
        print("Calibration Okay")
    else:
        print("Calibration error: sample flow rate out of bounds")  

    # Find sample flow rate
    flowCheckStartIndex = valveChangesIndex[2 * _NUM_CAL_CHECK_STEPS] + _FLOW_START_BUFFER
    flowCheckEndIndex = valveChangesIndex[2 * _NUM_CAL_CHECK_STEPS + 1]
    sampleFlowData = data['Sensirion_Liq_Flow (MANIFOLD)'][flowCheckStartIndex : flowCheckEndIndex]
    sampleFlowMedian = statistics.median(sampleFlowData)
    sampleFlowMax = sampleFlowMedian    # Inital value
    sampleFlowMin = sampleFlowMedian    # Inital value

    # Iterate through flow rate to see if certain interruptions in flow are allowable errors (caused by small bubbles)
    for index, flowRate in enumerate(sampleFlowData):
        if abs((flowRate - sampleFlowMedian) / sampleFlowMedian) > _FLOW_ERR_TOL_MAG:                   # Check if flow rate is outside tolerance
            endLoc = index + _FLOW_ERR_TOL_PERIOD
            if endLoc < len(sampleFlowData):                                                            # Make sure index is within bounds        
                badFlowCount = 0                   
                for flowRate_sub in sampleFlowData[index:endLoc]:                                       # Count number of flow rate errors in local subsection
                    if abs((flowRate_sub - sampleFlowMedian) / sampleFlowMedian) > _FLOW_ERR_TOL_MAG:
                        badFlowCount = badFlowCount + 1                      
                if badFlowCount / _FLOW_ERR_TOL_PERIOD < _FLOW_ERR_TOL_DUTY:                            # Check if number of errors is allowable
                    continue                                                                            # If allowable, do not update min or max values            
        # Update min and max values if applicable
        if flowRate > sampleFlowMax:
            sampleFlowMax = flowRate
        elif flowRate < sampleFlowMin:
            sampleFlowMin = flowRate

    maxDeviation = max(sampleFlowMedian - sampleFlowMin, sampleFlowMax - sampleFlowMedian) / sampleFlowMedian * 100
    print(f"\nSample Flow Rate: {round(sampleFlowMedian,2)} uL/min, {round(maxDeviation,2)}% maximum deviation from median")                    

    # Set up plot
    plt.close('all')
    fig,axs = plt.subplots(nrows=2,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('Sample Flow Rate Test')
    fig.suptitle("Sample Flow Rate Test - " + os.path.basename(CSVfileName), fontsize=14)
    
    # Plot calibration check on subplot 0
    axs[0].set_title('Rotary Valve Calibration Check')
    axs[0].set_ylabel('Flow Rate (uL/min)')
    axs[0].set_xlabel('Time (sec)')

    calCheckStartTime = data['Time (sec)'][valveChangesIndex[0]]
    calCheckEndTime = data['Time (sec)'][valveChangesIndex[_NUM_CAL_CHECK_STEPS * 2 - 1]]
    axs[0].set_xlim(calCheckStartTime, calCheckEndTime)
 
    lns1 = axs[0].plot(data['Time (sec)'], data['Sensirion_Liq_Flow (MANIFOLD)'], 'C0', label = 'Flow Rate')

    # Plot rotary valve position on secondary axis
    axs_rotary = axs[0].twinx()
    axs_rotary.set_ylabel('Rotary valve position (°)')
    lns2 = axs_rotary.plot(data['Time (sec)'], data['ROT_VLV_Position (OPTICS)'], 'C1--', label = 'Rotary Valve Position') 
    
    # Format legend
    lns = lns1 + lns2
    labs = [x.get_label() for x in lns]
    plt.legend(lns, labs, loc='upper left', bbox_to_anchor=(0,1), bbox_transform=axs[0].transAxes)

    # Plot sample flow rate on subplot 1
    axs[1].set_title('Sample Flow Rate')
    axs[1].set_ylabel('Flow Rate (uL/min)')
    axs[1].set_xlabel('Time (sec)')

    flowCheckStartTime = data['Time (sec)'][flowCheckStartIndex]
    flowCheckEndTime = data['Time (sec)'][flowCheckEndIndex]

    axs[1].set_xlim(flowCheckStartTime, flowCheckEndTime)
    axs[1].set_ylim(sampleFlowMin - sampleFlowMedian * _PLOT_Y_BUFFER, sampleFlowMax + sampleFlowMedian * _PLOT_Y_BUFFER)

    fitData = pandas.DataFrame(data={'Time (sec)':[flowCheckStartTime, flowCheckEndTime], 'Flow Rate Median':[sampleFlowMedian, sampleFlowMedian]})
    axs[1].plot(data['Time (sec)'], data['Sensirion_Liq_Flow (MANIFOLD)'], 'C0', label = 'Flow Rate')
    axs[1].plot(fitData['Time (sec)'], fitData['Flow Rate Median'], 'C1--', label = 'Flow Rate Median')
    
    axs[1].legend()

    plt.show()
    print("\n")

    # Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fluid flow rate test results')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)