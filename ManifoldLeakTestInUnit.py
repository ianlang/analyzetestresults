import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):

    # Global definitions
    _GENERIC_TRANSITION_BUFFER = 10
    _BALLAST_TEST_TRANSITION_BUFFER = 2000
    _BALLAST_TEST_START_INDEX = 2
    _BALLAST_TEST_END_INDEX = 3

    # Read in data, convert valve mask from hex to integer
    data = pandas.read_csv(CSVfileName, converters={'Pneum_Valve_Mask_En (MANIFOLD)': lambda x: int(x, 16)})

    # Find transitions between test stages
    stageChanges = [x for x, i in enumerate(data['Press_PID_Enable (MANIFOLD)'].diff()) if i != 0]

    # Find where valves change states
    valveStateChanges = [x for x, i in enumerate(data['Pneum_Valve_Mask_En (MANIFOLD)'].diff()) if i != 0]
    for change in valveStateChanges:
        if change > stageChanges[_BALLAST_TEST_START_INDEX]:
            stageChanges.append(change)
            break

    # Calculate leak rate during ballast test
    startBallastPres = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_START_INDEX] + _BALLAST_TEST_TRANSITION_BUFFER]
    startBallastTime = data['Time (sec)'][stageChanges[_BALLAST_TEST_START_INDEX] + _BALLAST_TEST_TRANSITION_BUFFER]
    endBallastPres = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_END_INDEX] - _GENERIC_TRANSITION_BUFFER]
    endBallastTime = data['Time (sec)'][stageChanges[_BALLAST_TEST_END_INDEX] - _GENERIC_TRANSITION_BUFFER]
    avgBallastLeakRate = round((startBallastPres - endBallastPres) / (startBallastTime - endBallastTime),3)


    # Print out results to console
    print(f'Ballast leak rate: {abs(avgBallastLeakRate)} mbar / sec')

    ballastTestMinPressure = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_START_INDEX]:stageChanges[_BALLAST_TEST_END_INDEX]].min()
    ballastTestMaxPressure = data['Press_Ballast (MANIFOLD)'][stageChanges[_BALLAST_TEST_START_INDEX]:stageChanges[_BALLAST_TEST_END_INDEX]].max()

    # Plotting data
    plt.close('all')
    fig,axs = plt.subplots(nrows=1,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('In-Unit Manifold Test')
    fig.suptitle("In-Unit Manifold Test - " + os.path.basename(CSVfileName), fontsize=14)

    # Plot ballast test
    axs.set_title('Ballast Leak Test')
    axs.set_ylabel('Pressure (mbar)')
    axs.set_xlabel('Time (sec)')
    data.plot(
        ax = axs, 
        x='Time (sec)', 
        y ='Press_Ballast (MANIFOLD)', 
        xlim = [data.at[stageChanges[_BALLAST_TEST_START_INDEX],'Time (sec)'], data.at[stageChanges[_BALLAST_TEST_END_INDEX],'Time (sec)']], 
        ylim = [ballastTestMinPressure - 2, ballastTestMaxPressure + 2],
        label = 'Ballast Pressure'
    )
    ballastData = pandas.DataFrame(data={'Time (sec)':[startBallastTime, endBallastTime], 'Pressure':[startBallastPres, endBallastPres]})
    ballastData.plot(
        ax = axs,
        x='Time (sec)',
        y='Pressure',
        label='Ballast Pressure (fitted)'
    )
    axs.legend(loc='upper right')

    plt.show()

    print("\n")


# Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analyze Manifold Performance')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)