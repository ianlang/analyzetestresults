import os
import pandas
import sys
import argparse
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

# Supress depreciation warning on MATPLOTLIBDATA
import warnings
warnings.filterwarnings("ignore", "(?s).*MATPLOTLIBDATA.*")

def processTest(CSVfileName):
    # Global definitions
    _PICKOFF_NUM_POINTS_TO_AVG = 25
    _LED_CURRENT_BUFFER = 25
    _LED_CURRENT_MAX_POS_ERROR = .15
    _LED_CURRENT_MAX_NEG_ERROR = -.05
    _LED_MAX_ALLOWABLE_TEMP = 50

    # Read in data
    data = pandas.read_csv(CSVfileName)

    # Find locations where LED current changes
    LEDChangesIndex = [i for i, setpoint in enumerate(data['UV_LED_Iset_1 (OPTICS)'].diff()) if setpoint > 0]
    LEDChangesIndex.insert(0,0) # Insert point at beginning

    LEDSetpoints = []
    UVPickoffData = []
    LEDCurrents = []
    print("UV Pickoff Results:")
    for index in LEDChangesIndex:
        # Find setpoint
        setpoint = data['UV_LED_Iset_1 (OPTICS)'][index]
        LEDSetpoints.append(setpoint)

        # Find UV pickoff value
        UVPickoffSubset = data['UV_Pickoff (OPTICS)'][index : index + _PICKOFF_NUM_POINTS_TO_AVG]
        UVPickoff = statistics.mean(UVPickoffSubset)
        UVPickoffData.append(UVPickoff)

        # Find LED current and verify error is acceptable
        current = data['UV_LED_Imon (OPTICS)'][index + _LED_CURRENT_BUFFER]
        LEDCurrents.append(current)
        if setpoint != 0:
            LEDCurrentError = (current / 5 - setpoint) / setpoint
            if (LEDCurrentError > _LED_CURRENT_MAX_POS_ERROR or LEDCurrentError < _LED_CURRENT_MAX_NEG_ERROR):
                print(f"WARNING: LED current out of range, measured:{round(current/5,1)} mA, setpoint: {setpoint} mA")

        # Log data to console
        print(f'\t{setpoint} mA: {round(UVPickoff, 2)} V')

    UVPickoffFitLine = np.polyfit(LEDSetpoints, UVPickoffData, 1)
    print(f"Pickoff Sensitivity: {round(UVPickoffFitLine[0],3)} V/mA")

    # Detect and log LED temperature
    LEDMaxTemp = max(data['UV_LED_Temp (OPTICS)'])
    print(f"LED starting temperature: {data['UV_LED_Temp (OPTICS)'][0]} °C")
    print(f"LED max temperature: {LEDMaxTemp} °C")

    if(LEDMaxTemp > _LED_MAX_ALLOWABLE_TEMP):
        print("WARNING: LED Temperature too high")

    # Plotting data
    plt.close('all')
    fig,axs = plt.subplots(nrows=3,ncols=1,figsize=(10, 6),constrained_layout=True)
    fig.canvas.set_window_title('UV LED Pickoff Test')
    fig.suptitle("UV LED Pickoff Test - " + os.path.basename(CSVfileName), fontsize=14)

    # Plot UV Pickoff on subplot 0
    axs[0].set_title('LED Setpoint')
    axs[0].set_ylabel('UV Pickoff Reading (V)')
    axs[0].set_xlabel('Time (sec)')
    lns1 = axs[0].plot(data['Time (sec)'], data['UV_Pickoff (OPTICS)'], 'C0', label = 'UV Pickoff')

    # Plot LED setpoint and current on secondary axis
    axs_setpoint = axs[0].twinx()
    axs_setpoint.set_ylabel('LED Current per bank (mA)')
    lns2 = axs_setpoint.plot(data['Time (sec)'], [x/5 for x in data['UV_LED_Imon (OPTICS)']], 'C1--', label = 'UV LED Current') 
    lns3 = axs_setpoint.plot(data['Time (sec)'], data['UV_LED_Iset_1 (OPTICS)'], 'C2--', label = 'UV LED Setpoint') 

    lns = lns1 + lns2 + lns3
    labs = [x.get_label() for x in lns]
    plt.legend(lns, labs, loc='upper left', bbox_to_anchor=(0,1), bbox_transform=axs[0].transAxes)


    # Plot LED temperature on subplot 1
    axs[1].set_title('LED Temperature')
    axs[1].set_ylabel('Temperature (°C)')
    axs[1].set_xlabel('Time (sec)')
    axs[1].plot(data['Time (sec)'], data['UV_LED_Temp (OPTICS)'], 'C0', label = 'LED Temperature')
    axs[1].legend()

    # Plot UV pickoff vs setpoint on subplot 2
    axs[2].set_title('LED Temperature')
    axs[2].set_ylabel('UV Pickoff (V)')
    axs[2].set_xlabel('LED Setpoint (mA)')
    axs[2].plot(LEDSetpoints, UVPickoffData, 'C0--o', label = 'UV Pickoff')
    axs[2].legend()

    plt.show()
    print("\n")


# Allow for running python module directly on a input csv file
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Mobilizer flow rate test results')
    parser.add_argument('inputCSV', type=argparse.FileType('r'))
    args = parser.parse_args()
    processTest(args.inputCSV.name)